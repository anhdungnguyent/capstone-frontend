import Table from 'react-bootstrap/Table';
import Header from '../layout/Header';
import Sidebar from '../layout/SideBar';
import Dropdown from 'react-bootstrap/Dropdown';
import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import ReactPaginate from 'react-paginate';

// Be sure to include styles at some point, probably during your bootstraping
import '@trendmicro/react-paginations/dist/react-paginations.css';


const ShiftList = () => {

    const options = [
        'Nguyen Van A',
        'Tran Van B',
        'Le Thi C'
    ];

    const defaultOption = options[0];

    const onSelect = () => {

    };

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [show2, setShow2] = useState(false);

    const handleClose2 = () => setShow2(false);
    const handleShow2 = () => setShow2(true);

    const [show3, setShow3] = useState(false);

    const handleClose3 = () => setShow3(false);
    const handleShow3 = () => setShow3(true);

    const handleShow4 = () => {
        setShow(false);
        setShow2(true);
    }

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <a
            href=""
            ref={ref}
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}
            style={{ color: '#000000' }}
        >
            {children}
        </a>
    ));

    const data = [
        {
            id: 1,
            code: 'JHDAJSH',
            email: 'johndoe@example.com',
            type: 'Yêu cầu',
            status: 'Đã xử lý',
            notes: 'Culpa fugiat eu est aute proident ea voluptate dolore.',
            shift: 'ca sáng'
        },
        {
            id: 2,
            code: 'DASFAF',
            email: 'janedoe@example.com',
            type: 'Yêu cầu',
            status: 'Đã xử lý',
            notes: 'Adipisicing ut fugiat ea tempor excepteur officia excepteur deserunt do irure proident ipsum exercitation.',
            shift: 'ca chiều'
        }
    ];

    function Items({ currentItems }) {
        return (
            <>
                {currentItems &&
                    currentItems.map((item) => (
                        <div>
                            <h3>Item #{item}</h3>
                        </div>
                    ))}
            </>
        );
    }

    return (
        <div className="container-fluid w-100 row p-0 m-0" style={{ backgroundColor: '#F5F5F5' }}>
            <Header />
            <div className="row">
                <Sidebar />
                <div className='col-lg-10 col-md-8 col-12 ps-5' style={{ fontSize: '14px', lineHeight: '20px' }}>
                    <div className='my-5'>
                        <h1>Quản lý ca làm</h1>
                    </div>
                    <div className='w-100 '>
                        <div className='d-flex justify-content-between align-items-center bg-white list-header p-4'>
                            <h2 className='col-lg-6'>Danh sách ca làm</h2>
                            <div className='d-flex justify-content-end col-lg-6'>
                                <button className='btn btn-danger ms ms-4' style={{ fontSize: '14px' }} onClick={handleShow3}>Chỉnh sửa</button>
                                <Modal show={show3} onHide={handleClose3}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Tạo kho</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="primary" onClick={handleClose3}>
                                            Tạo yêu cầu
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </div>
                        </div>
                        <div className='bg-white p-4'>
                            <Table responsive bordered striped cellSpacing={0}>
                                <thead>
                                    <tr>
                                        <th style={{ width: '4rem' }}>CA LÀM</th>
                                        <th style={{ width: '4rem' }}>THỨ HAI</th>
                                        <th style={{ width: '4rem' }}>THỨ BA</th>
                                        <th style={{ width: '4rem' }}>THỨ TƯ</th>
                                        <th style={{ width: '4rem' }}>THỨ NĂM</th>
                                        <th style={{ width: '4rem' }}>THỨ SÁU</th>
                                        <th style={{ width: '4rem' }}>THỨ BẢY</th>
                                        <th style={{ width: '4rem' }}>CHỦ NHẬT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr height='30px'>
                                        <td>CA SÁNG</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                    </tr>
                                    <tr height='30px'>
                                        <td>CA TỐI</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                        <td>Nguyễn Văn A<br />Trần Văn B<br />Phạm Thành C</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    )
}

export default ShiftList;