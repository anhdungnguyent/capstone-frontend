import React from "react";

const Notification = ({ message }) => (
  <div className="notification py-3 px-4">
    <p className="notiTitle"><i class="fa-solid fa-circle-check"></i> THÔNG BÁO</p>
    <p className="notiContent">Bạn đã tạo thành công {message} mã QR</p>
  </div>
);

export default Notification;