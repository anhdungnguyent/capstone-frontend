import { useRef, useState } from "react";

const EditWarehouse = () => {

    const [requestId, setRequestId] = useState();
    const requestRef = useRef();
    const [orderId, setOrderId] = useState();
    const orderRef = useRef();

    return (
        <>
            <div className="">
                <section>
                    <div className="mb-3 edit-modal">
                        <label htmlFor="whId" className="edit-label-modal mb-3">MÃ KHO <span style={{color: 'red'}}>*</span></label>
                        <input
                            type="text"
                            className="form-control"
                            id="whId"
                            name="whId"
                            ref={requestRef}
                            autoComplete="off"
                            onChange={(e) => setRequestId(e.target.value)}
                            value={requestId}
                            aria-describedby="whId"
                            style={{ padding: '8px 12px' }}
                            required
                        />
                    </div>
                    <div className="mb-3 edit-modal">
                        <label htmlFor="name" className="edit-label-modal mb-3">TÊN KHO <span style={{color: 'red'}}>*</span></label>
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="name"
                            style={{ padding: '8px 12px' }}
                            required
                        />
                    </div>
                    <div className="mb-3 edit-modal">
                        <label htmlFor="address" className="edit-label-modal mb-3">ĐỊA CHỈ <span style={{color: 'red'}}>*</span></label>
                        <input
                            type="text"
                            className="form-control"
                            id="address"
                            name="address"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="address"
                            style={{ padding: '8px 12px' }}
                            required
                        />
                    </div>
                </section>
            </div>
        </>
    )

}

export default EditWarehouse;