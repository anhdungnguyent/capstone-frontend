import { useRef, useState } from "react";

const CreateWarehouse = () => {

    const [requestId, setRequestId] = useState();
    const requestRef = useRef();
    const [orderId, setOrderId] = useState();
    const orderRef = useRef();

    return (
        <>
            <div className="">
                <section>
                    {/* <p
                            ref={errRef}
                            className={errMsg ? "errmsg" : "offscreen"}
                            aria-live="assertive"
                        >
                            {errMsg}
                        </p> */}
                    <div className="mb-3 edit-modal">
                        <label htmlFor="requestId" className="edit-label-modal mb-3">MÃ KHO <span style={{color: 'red'}}>*</span></label>
                        <input
                            type="text"
                            className="form-control"
                            id="requestId"
                            name="requestId"
                            ref={requestRef}
                            autoComplete="off"
                            onChange={(e) => setRequestId(e.target.value)}
                            value={requestId}
                            aria-describedby="requestId"
                            placeholder="Nhập mã yêu cầu"
                            style={{ padding: '8px 12px' }}
                            required
                        />
                    </div>
                    <div className="mb-3 edit-modal">
                        <label htmlFor="orderId" className="edit-label-modal mb-3">TÊN KHO <span style={{color: 'red'}}>*</span></label>
                        <input
                            type="orderId"
                            className="form-control"
                            id="orderId"
                            name="orderId"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="orderId"
                            placeholder="Nhập đơn hàng"
                            style={{ padding: '8px 12px' }}
                            required
                        />
                    </div>
                    <div className="mb-3 edit-modal">
                        <label htmlFor="orderId" className="edit-label-modal mb-3">ĐỊA CHỈ <span style={{color: 'red'}}>*</span></label>
                        <input
                            type="orderId"
                            className="form-control"
                            id="orderId"
                            name="orderId"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="orderId"
                            placeholder="Nhập đơn hàng"
                            style={{ padding: '8px 12px' }}
                            required
                        />
                    </div>
                </section>
            </div>
        </>
    )

}

export default CreateWarehouse;