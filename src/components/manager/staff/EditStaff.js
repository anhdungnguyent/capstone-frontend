import { useRef, useState } from "react";

const EditStaff = () => {

    const [requestId, setRequestId] = useState();
    const requestRef = useRef();
    const [orderId, setOrderId] = useState();
    const orderRef = useRef();

    return (
        <>
            <div className="">
                <section>
                    <div className="mb-3 input-modal">
                        <label htmlFor="staffId" className="input-label-modal mb-3">MÃ NHÂN VIÊN</label>
                        <input
                            type="text"
                            className="form-control"
                            id="staffId"
                            name="staffId"
                            ref={requestRef}
                            autoComplete="off"
                            onChange={(e) => setRequestId(e.target.value)}
                            value={requestId}
                            aria-describedby="staffId"
                            style={{padding: '8px 12px'}}
                            disabled
                        />
                    </div>
                    <div className="mb-3 input-modal">
                        <label htmlFor="staffName" className="input-label-modal mb-3">TÊN NHÂN VIÊN</label>
                        <input
                            type="text"
                            className="form-control"
                            id="staffName"
                            name="staffName"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="staffName"
                            style={{padding: '8px 12px'}}
                            disabled
                        />
                    </div>
                    <div className="mb-3 input-modal">
                        <label htmlFor="email" className="input-label-modal mb-3">EMAIL</label>
                        <input
                            type="email"
                            className="form-control"
                            id="email"
                            name="email"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="email"
                            style={{padding: '8px 12px'}}
                            disabled
                        />
                    </div>
                    <div className="mb-3 input-modal">
                        <label htmlFor="phone" className="input-label-modal mb-3">SỐ ĐIỆN THOẠI</label>
                        <input
                            type="tel"
                            className="form-control"
                            id="phone"
                            name="phone"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="phone"
                            style={{padding: '8px 12px'}}
                            disabled
                        />
                    </div>
                    <div className="mb-3 input-modal">
                        <label htmlFor="warehouse" className="input-label-modal mb-3">KHO</label>
                        <input
                            type="text"
                            className="form-control"
                            id="warehouse"
                            name="warehouse"
                            ref={orderRef}
                            autoComplete="off"
                            onChange={(e) => setOrderId(e.target.value)}
                            value={orderId}
                            aria-describedby="warehouse"
                            style={{padding: '8px 12px'}}
                            disabled
                        />
                    </div>
                    <div className="mb-3 input-modal">
                        <label htmlFor="type" className="input-label-modal mb-3">TÌNH TRẠNG</label>
                        <select id="type" className='w-100' style={{padding: '8px 12px'}}>
                            <option value={"1"}>Đã xử lý</option>
                            <option value={"2"}>Đang xử lý</option>
                            <option value={"3"}>Đang chờ xử lý</option>
                            <option value={"4"}>Đã huỷ</option>
                        </select>
                    </div>
                </section>
            </div>
        </>
    )

}

export default EditStaff;