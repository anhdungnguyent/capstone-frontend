import Header from "../../layout/Header";
import Sidebar from "../../layout/SideBar";
import React, { useEffect, useState } from 'react';
import { managerStaffReportTableApi } from '../../apiCaller/services.js';
import { Table } from "react-bootstrap";
import ReactPaginate from "react-paginate";

const MOrderReport = () => {

    const accessToken = localStorage.getItem("accessToken");
    const [staffList, setStaffList] = useState([]);
    const [pageCount, setPageCount] = useState(1);
    const [month, setMonth] = useState(Number((new Date()).getMonth() + 1));

    const fetchStaff = async (page, month) => {
        try {
            const staffData = await managerStaffReportTableApi(page, month, accessToken);
           
            setStaffList(staffData?.data);
            setPageCount(staffData?.totalPage);
        } catch (err) {

        }
    }

    useEffect(() => {
        fetchStaff(1, month);
    }, []);

    const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
        <a
            href=""
            ref={ref}
            onClick={(e) => {
                e.preventDefault();
                onClick(e);
            }}
            style={{ color: '#000000' }}
        >
            {children}
        </a>
    ));

    const handlePageClick = (event) => {
        fetchStaff(+event.selected + 1, month);
    };

    let rows2;

    if (staffList && staffList.length < 10) {
        rows2 = (
            <>
                {
                    [...Array(10 - staffList.length)].map((_, i) => (
                        <tr style={{ height: '30px' }} key={staffList.length + i}>
                            <td></td>
                            <td></td>
                        </tr>
                    ))
                }
            </>
        );
    };

    return (
        <section>
            <div className="container-fluid w-100 row p-0 m-0" style={{ backgroundColor: '#FAFAFA' }}>
                <Header />
                <div className="row">
                    <Sidebar />
                    <div className="col-lg-10 col-md-8 p-5">
                        <div className="d-flex my-5">
                            <h1 className=''><a href="#/report-manager">Báo cáo</a> &gt;&nbsp;</h1><h2 className="p-2">Nhân viên vận chuyển</h2>
                        </div>
                        <div>
                            <div className='mx-5 my-3 row'>
                                <div className="col-lg-6"></div>
                                <div className='d-md-flex flex-row-reverse col-lg-6 py-4'>
                                    <div className="ms-0 ms-md-4 d-flex justify-content-end">
                                        <select
                                            value={month}
                                            onChange={e => {
                                                setMonth(Number(e.target.value));
                                                fetchStaff(1, Number(e.target.value));
                                            }}
                                            className='p-2 mt-md-0 mt-3'
                                            style={{ fontSize: '16px' }}
                                        >
                                            <option value={"1"}>Tháng 1</option>
                                            <option value={"2"}>Tháng 2</option>
                                            <option value={"3"}>Tháng 3</option>
                                            <option value={"4"}>Tháng 4</option>
                                            <option value={"5"}>Tháng 5</option>
                                            <option value={"6"}>Tháng 6</option>
                                            <option value={"7"}>Tháng 7</option>
                                            <option value={"8"}>Tháng 8</option>
                                            <option value={"9"}>Tháng 9</option>
                                            <option value={"10"}>Tháng 10</option>
                                            <option value={"11"}>Tháng 11</option>
                                            <option value={"12"}>Tháng 12</option>
                                        </select>
                                    </div>
                                    <div className="d-flex justify-content-end mt-3 mt-md-0">
                                        <ReactPaginate
                                            breakLabel="..."
                                            nextLabel=">"
                                            onPageChange={handlePageClick}
                                            pageRangeDisplayed={20}
                                            pageCount={pageCount}
                                            previousLabel="<"
                                            renderOnZeroPageCount={null}
                                            marginPagesDisplayed={2}
                                            pageClassName="page-item"
                                            pageLinkClassName="page-link"
                                            previousClassName="page-item"
                                            previousLinkClassName="page-link"
                                            nextClassName="page-item"
                                            nextLinkClassName="page-link"
                                            breakClassName="page-item"
                                            breakLinkClassName="page-link"
                                            containerClassName="pagination"
                                            activeClassName="active"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className='' style={{ backgroundColor: '#FFFFFF' }}>
                                <Table responsive bordered cellSpacing={0}>
                                    <thead>
                                        <tr>
                                            <th style={{ width: '15%' }}>SHIPPER</th>
                                            <th style={{ width: '15%' }}>SỐ ĐƠN NHẬN</th>
                                            <th style={{ width: '15%' }}>SỐ ĐƠN GIAO</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {staffList.map((item, index) => (
                                            <tr height='30px' key={`request-${index}`}>
                                                <td>{item?.staffName}</td>
                                                <td>{item?.totalPickupOrder}</td>
                                                <td>{item?.totalDeliverOrder}</td>
                                            </tr>
                                        ))}
                                        {rows2}
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    )
}

export default MOrderReport;