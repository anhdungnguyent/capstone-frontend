import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { adminOrderReportGraphApi } from '../../apiCaller/services';
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

function OrderChart() {

  const accessToken = localStorage.getItem("accessToken");

  const [orderReportGraph, setOrderReportGraph] = useState([]);

  const fetchOrderReportGraph = async () => {
    try {
      const rgData = await adminOrderReportGraphApi(accessToken);
      const newArray = [];
      rgData.map((item) => (
        newArray.push(item?.totalOrder)
      ))
      setOrderReportGraph(newArray);
    } catch (err) {

    }
  }

  useEffect(() => {
    fetchOrderReportGraph();
  }, [])


  const options = {
    responsive: true,
    plugins: {
      legend: {
        // position: 'top' as const,
      },
      title: {
        display: true,
        text: 'Báo cáo đơn hàng',
      },
    },
    scales: {
      y: {
        beginAtZero: true,
        display: true,
        ticks: {
          maxTicksLimit: 8
        }
      }
    }
  };

  const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  const data = {
    labels,
    datasets: [
      {
        label: 'Đơn hàng',
        data: orderReportGraph,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
    ],
  };

  return (
    <Line options={options} data={data} />
  );
}
export default OrderChart;
