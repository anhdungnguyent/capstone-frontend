import React from "react";
import MainManagement from "./MainManagement";
import Header from "../layout/Header";
import Sidebar from "../layout/SideBar";

class AdminManagement extends React.Component {
    render() {
        return (
            <div className="container-fluid w-100 row p-0 m-0" style={{backgroundColor: '#2D9CDB'}}>
                <Header/>
                <div className="row">
                    <Sidebar />
                    <MainManagement/>
                </div>
                
            </div>
        );
    }
}

export default AdminManagement