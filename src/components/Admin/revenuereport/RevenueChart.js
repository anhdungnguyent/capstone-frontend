import React, { useEffect, useState } from 'react';
import { adminRevenueGraphReportApi } from '../../apiCaller/services.js';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

function RevenueChart() {

  const accessToken = localStorage.getItem("accessToken");

  const [revenueGraph, setRevenueGraph] = useState([]);

  const fetchRevenueGraph = async () => {
    try {
      const rgData = await adminRevenueGraphReportApi(accessToken);
      const newArray = [];
      rgData.map((item) => (
        newArray.push(item?.totalRevenue)
      ))
      setRevenueGraph(newArray);
    } catch (err) {

    }
  }

  useEffect(() => {
    fetchRevenueGraph();
  }, [])

  const options = {
    responsive: true,
    plugins: {
      legend: {
        // position: 'top' as const,
      },
      title: {
        display: true,
        text: 'Báo cáo doanh thu',
      },
    },
    scales: {
      y: {
        beginAtZero: true,
        display: true,
        ticks: {
          maxTicksLimit: 8
        }
      }
    }
  };

  const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  const data = {
    labels,
    datasets: [
      {
        label: 'Doanh thu',
        data: revenueGraph,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
    ],
  };

  return (
    <Line options={options} data={data} />
  );

}
export default RevenueChart;
