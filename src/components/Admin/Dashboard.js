import React from "react";
import Header from "../layout/Header";
import SplineAreaChart from "./report/SplineAreaChart";
import Sidebar from "../layout/SideBar";

class Dashboard extends React.Component {
    render() {
        return (
            <div className="container-fluid w-100 row p-0 m-0" style={{ backgroundColor: '#2D9CDB' }}>
                <Sidebar />
                <div className="col-md-9 col-sm-12">
                    <Header />
                    <div className="row">
                        <SplineAreaChart />
                    </div>
                </div>

            </div>
        );
    }
}

export default Dashboard