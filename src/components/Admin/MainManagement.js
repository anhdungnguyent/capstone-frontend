
import "react-bootstrap/dist/react-bootstrap.min.js";
import { getAllAccountApi } from "../apiCaller/services";
import { useEffect, useState } from "react";

export default function MainManagement() {

    const [listAccounts, setListAccounts] = useState([]);

    useEffect(() => {
        getAccounts().then(data => setListAccounts(data.data));
    }, [])

    const getAccounts = async () => {
        // try {
        //     const response = await fetch('https://dev.api.h2h.just.engineer/account/getAll',
        //     {
        //         headers: {
        //             "Content-Type": "application/json",
        //           }
        //     });
        //     const data = response.json();
        //     return data;
        // } catch (error) {
        //     console.error("Fetching error", error);
        // }
        let res = await getAllAccountApi();
        if (res && res.data) {
            return res;
        }
    }

    return (
        // <Container fluid className="p-5 col-9">
        //   <ReactTableScroll>
        //     <table
        //       width="100%"
        //       className="table table-bordered table-hover table-striped"
        //     >
        //       <thead>
        //         <tr>
        //           <td>ID</td>
        //           <td width="5%">Name</td>
        //           <td width="5%">Last&nbsp;Name</td>
        //           <td width="5%">Surname</td>
        //           <td width="25%">Passport&nbsp;ID</td>
        //           <td width="25%">Company</td>
        //           <td width="25%">Address</td>
        //           <td width="25%">Country</td>
        //         </tr>
        //       </thead>
        //       <tbody>
        //         {Array(100)
        //           .fill(null)
        //           .map((item, index) => (
        //             <tr key={index}>
        //               <td>{index}</td>
        //               <td>Michel</td>
        //               <td>James</td>
        //               <td>Jackson</td>
        //               <td>{Math.ceil(Math.random() * 1000000)}</td>
        //               <td>Big&nbsp;Hot&nbsp;Dog</td>
        //               <td>Ap&nbsp;#190-9925&nbsp;n&nbsp;Road</td>
        //               <td>USA,&nbsp;Florida</td>
        //             </tr>
        //           ))}
        //       </tbody>
        //     </table>
        //   </ReactTableScroll>
        // </Container>

        <div className="p-3 col-sm-9">

            <table id="example" className="table table-striped bg-white" style={{ width: '100%' }}>
                <thead>
                    <tr>
                        <th>User Name</th>
                        <th>Role</th>
                        <th>isActive</th>
                        <th>created date</th>
                        <th>modified date</th>
                    </tr>
                </thead>
                <tbody>
                    {listAccounts && listAccounts.length > 0 && listAccounts.map((item, index) => {
                        return (
                            <tr key={`acc-${index}`}>
                                <td>{item.username}</td>
                                <td>{item.role.role_name}</td>
                                <td>{item.first_name}</td>
                                <td>{item.date_create_at}</td>
                                <td>{item.date_update_at}</td>
                            </tr>
                        )
                    })}

                </tbody>
            </table>
        </div>
    );
}
